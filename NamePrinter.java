import java.util.Scanner;

public class NamePrinter {
    public static void printName() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Nama anda: ");
        String name = scanner.nextLine();

        for (int i = 0; i < name.length(); i++) {
            System.out.println(String.format("Karakter[%d]: %s", i + 1, name.charAt(i)));
        }

        scanner.close();

    }
}
