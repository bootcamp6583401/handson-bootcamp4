public class GCDemo {
    public static void run() {
        displayMemoryUsage("Initial memory usage (before):");

        for (int i = 1; i <= 5; i++) {
            String bro = "Halo " + "Apa " + "Kabar";
            String[] letters = bro.split("");
            for (String string : letters) {
                System.out.println(string);
            }

            bro = null;
        }

        displayMemoryUsage("Initial memory usage (after):");

        System.gc();

        displayMemoryUsage("Memory usage after creating and performing operations on Hamster objects in a loop:");

    }

    public static void runBetter() {
        displayMemoryUsage("Initial memory usage (before):");

        for (int i = 1; i <= 1000; i++) {
            String bro = "Bro bro bro";
            String[] letters = bro.split("");
            for (String string : letters) {
                System.out.println(string);
            }

            bro = null;
        }

        displayMemoryUsage("Initial memory usage (after):");

        System.gc();

        displayMemoryUsage("Memory usage after creating and performing operations on Hamster objects in a loop:");

    }

    private static void displayMemoryUsage(String message) {
        Runtime runtime = Runtime.getRuntime();

        long usedMemory = runtime.totalMemory() - runtime.freeMemory();

        System.out.println(message + " Used memory (bytes): " + usedMemory);
    }
}
