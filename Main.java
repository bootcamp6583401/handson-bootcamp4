public class Main {
    public static void main(String[] args) {

        // HANDSON 1 & 2
        /*
         * Sample bytcode
         * new: Buat instansi kelas.
         * dup: Duplikasi value di atas operand stack.
         * getstatic: Ambil nilai static (method printname).
         * invokespecial: Invokasi method (println).
         * astore_0: simpan operan atas di variable local index 0.
         */
        NamePrinter.printName();

        // Handoson 3
        // GCDemo.run();
        GCDemo.runBetter();
    }
}
